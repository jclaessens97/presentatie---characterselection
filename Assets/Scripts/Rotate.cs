﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    // Offset to position 1 plane in the middle
    private readonly int OFFSET = 30;

    public GameObject Anchor;
    private readonly float RotationSpeed = 60f;
    private readonly float RotationDuration = 1f;

    void Start()
    {
        Anchor = GameObject.Find("Anchor");

        var rotation = transform.rotation;
        transform.RotateAround(Anchor.transform.position, Vector3.up, OFFSET);
        transform.rotation = rotation;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            StartCoroutine(nameof(RotateOnce), 1f);
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            StartCoroutine(nameof(RotateOnce), -1f);
        }
    }

    private IEnumerator RotateOnce(float direction)
    {
        float timeBegin = Time.time;
        float endTime = timeBegin + RotationDuration;

        while (Time.time < endTime)
        { 
            var localRotation = transform.rotation;
            transform.RotateAround(Anchor.transform.position, Vector3.up, direction * RotationSpeed * Time.deltaTime);
            transform.rotation = localRotation;
            yield return null;
        }
    }
}
