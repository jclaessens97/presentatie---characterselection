﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class SpawnManager : MonoBehaviour
{
    [Header("Role text settings")]
    private readonly string[] roles = new string[]
    {
        "Super admin",
        "Admin",
        "Moderator",
        "Ingelogde gebruiker",
        "Organisatie",
        "Anonieme gebruiker",
    };
    private int selectedRoleIndex = 0;
    public Text RoleText;

    [Header("Circle parameters")]
    public int NumberOfCharacters = 6;
    public GameObject Anchor;
    public List<GameObject> CharacterPrefabs;
    public float RadiusX = 7;
    public float RadiusZ = 7;

    void Start()
    {
        for (var characterNum = 0; characterNum < NumberOfCharacters; characterNum++)
        {
            // i is progress around the circle
            // We need a float so multiply by 1.0
            float i = characterNum * 1.0f / NumberOfCharacters;

            // Get the angle for this step in radians
            float angle = i * Mathf.PI * 2;

            // X & Y position, calculated with Sin and Cos
            float x = Mathf.Cos(angle) * RadiusX;
            float z = Mathf.Sin(angle) * RadiusZ;
            Vector3 pos = new Vector3(x, 0, z) + Anchor.transform.position;
            pos.y = 0.25f;
            Vector3 rot = new Vector3(90, 180, 0);

            // Spawn character prefabs
            Instantiate(CharacterPrefabs[characterNum], pos, Quaternion.Euler(rot));
        }

        RoleText.text = roles[selectedRoleIndex];
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            selectedRoleIndex++;
            if (selectedRoleIndex == roles.Length)
            {
                selectedRoleIndex = 0;
            }

            RoleText.text = roles[selectedRoleIndex];
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            selectedRoleIndex--;
            if (selectedRoleIndex < 0)
            {
                selectedRoleIndex = roles.Length - 1;
            }

            RoleText.text = roles[selectedRoleIndex];
        }
    }
}
